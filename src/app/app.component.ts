import { Component, ViewChild } from '@angular/core';
import { FormGroup, FormControl, FormArray, Validators, ValidatorFn } from '@angular/forms';
import { InputMultiComponent } from './input-multi/input-multi.component';

import { FormStringListArray } from './form-string-list-array'

interface formData {
  input: string;
  set: string;
  list: string[];
  list2: string[];
}

@Component({
  selector: 'app-root',
  template: `
    <form [formGroup]="form">
    <fieldset class="disabled" [disabled]="disabled">
      <label for="input">Input:</label>
        <input type="text" formControlName="input">
        <button type="button" (click)="addInput($event)">addInput()</button>
        <button type="button" (click)="setForm($event)">setForm()</button>
        <br>
        <label for="set">Set:</label>
        <input type="text" formControlName="set">
        <button type="button" (click)="set($event)">set(list)</button>
        <button type="button" (click)="clear($event)">clear()</button>
        <br>
        
        <app-input-multi 
          formArrayName="list"
          #list
          [validators]="listValidators"
          type="text"
          ></app-input-multi>
        <app-input-multi 
          #list2
          formArrayName="list2"
          ></app-input-multi>
      </fieldset>
    </form>
    <pre>ngModel:
{{form.value | json}}

Valid: {{form.valid}}</pre>
  `,
  styles: [".disabled {border: 0; padding: 0; margin: 0;}"]
})
export class AppComponent {
  @ViewChild('list') listCmp: InputMultiComponent;

  title = 'multi-formgroup';
  disabled = false;
  listValidators: ValidatorFn[] = [Validators.max(20), Validators.required]
  data = ['1','2'];

  form = new FormGroup({
    input: new FormControl("some text"),
    set: new FormControl("1,2,3"),
    list: new FormStringListArray([], this.listValidators),
    list2: new FormStringListArray(["1", "2"], this.listValidators)
  });

  ngOnInit() {
    // setting default values behaves ase I would expect, 
    // passing my form model into the form without having to 
    // construct the form array manually
    let default_data: formData = {
      input: "some text",
      set: "1,2,3",
      list: ["1", "2"],
      list2 : ["3", "4"]
    };

    //this.form.setValue(default_data);
  }

  setForm(event: Event) {
    this.form.get("list").setValue([new FormControl("1,2,3", this.listValidators)])
  }

  set(event: Event) {
    let v: string[] = this.form.get("set").value.split(",");
    this.listCmp.set(v)
  }

  clear(event: Event) {
    this.listCmp.clear()
  }

  addInput(event: Event) {
    this.listCmp.push(this.form.get("input").value);
  }
}
