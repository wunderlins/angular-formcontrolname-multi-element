import { FormArray, AbstractControl, FormControl, AbstractControlOptions, AsyncValidatorFn, ValidatorFn } from '@angular/forms';

/**
 * Overrides FormArray to allow passing in a string[] into setValue
 * 
 * @see class FormArray
 */
export class FormStringListArray extends FormArray {

    /**
     * 
     * @param controls list of controls or string list of values which will be embeded in FormControl()
     * @param validatorOrOpts A synchronous validator function, or an array of such functions, or an AbstractControlOptions object that contains validation functions and a validation trigger. Typically null or any of {updateOn: 'blur', validators: myValidator, asyncValidators: myAsyncValidator}
     * @param asyncValidator A single async validator or array of async validator functions
     */
    constructor(controls: AbstractControl[] | string[], validatorOrOpts?: ValidatorFn | AbstractControlOptions | ValidatorFn[], asyncValidator?: AsyncValidatorFn | AsyncValidatorFn[]) {
        if (controls.length && typeof controls[0] == "string") {
            let c: AbstractControl[] = [];
            super(c, validatorOrOpts, asyncValidator);
            this.setValue(controls)
        } else {
            super(controls as AbstractControl[], validatorOrOpts, asyncValidator);
        }
    }

    /**
     * allows to pass string[] values into a FormArray
     * 
     * With this implementation, all added sub components will inherit 
     * the validators of the FormArray if passed in as string[].
     * 
     * @param value accepts a list of FormControls or a list of Strings.
     * @param options @see class FormControl
     */
    setValue(value: string[] | AbstractControl[], options?: Object) {
        //console.log(value instanceof AbstractControl)
        if (value && value.length && value[0] instanceof AbstractControl) {
          while(this.length > 0)
            this.removeAt(0);
          for(let e in value) {
            //console.log(value[e])
            this.push(value[e] as AbstractControl)
          }
          return;
        }
  
        let l = this.length;
        let new_l = 0;
        if (!options) {
            options = {}
        }
        options["validators"] = this.validator
        for (let i=0; i<value.length; i++) {
            if (i < l) // replace
                this.setControl(i, new FormControl(value[i], options));
            else // append
                this.push(new FormControl(value[i], options));
            new_l++;
        }
    
        // trim remaining elements
        if (new_l < l) {
            for (let i=l; i>=new_l; i--) {
                this.removeAt(i);
            }
        }
    }

    push(value: AbstractControl | string): void {
        let ctrl: AbstractControl;
        if (value instanceof AbstractControl) {
          ctrl = value;
        } else {
          ctrl = new FormControl(value, this.validator);
        }
        
        super.push(ctrl)
    }

    insert(index: number, value: AbstractControl | string): void {
        let ctrl: AbstractControl;
        if (value instanceof AbstractControl) {
          ctrl = value;
        } else {
          ctrl = new FormControl(value, this.validator);
        }
        
        super.insert(index, ctrl);
    }
}
