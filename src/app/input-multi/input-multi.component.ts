import { Component, Host, Input, OnInit, Optional, SkipSelf } from '@angular/core';
import { AbstractControl, ControlValueAccessor, FormArray, FormControl, FormGroup, FormGroupDirective, NG_VALUE_ACCESSOR } from '@angular/forms';
import { FormStringListArray } from '../form-string-list-array'

// https://stackoverflow.com/questions/44731894/get-access-to-formcontrol-from-the-custom-form-component-in-angular
@Component({
  selector: 'app-input-multi',
  template: `
    <label>Array</label>
    <br>
    <div [formGroup]="formGroup">
      <div [formArrayName]="formArrayName" *ngFor="let l of formArray.controls; index as i">
        <input [formControlName]="i" [type]="type">
        <button type="button" (click)="insertAt(i+1, '')">+</button>
        <button type="button" (click)="removeAt(i)" *ngIf="i != 0">x</button>
      </div>
    </div>
  `,
  styles: [],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: InputMultiComponent,
    multi: true
  }]
})
export class InputMultiComponent implements ControlValueAccessor, OnInit {

  @Input() validators: any[]; // optional
  @Input() formArrayName: string;
  @Input() type: string = "text";

  formArray: FormStringListArray; // https://angular.io/api/forms/FormArray
  formGroup: FormGroup;
  
  constructor(@Optional() @Host() @SkipSelf()
    private controlContainer: FormGroupDirective) {}

  // OnInit implementation
  ngOnInit(): void {
    this.formGroup = this.controlContainer.form;
    this.formArray = this.controlContainer.control.get(this.formArrayName) as FormArray;

    // make sure we have at least one element in the array
    if (this.formArray.controls.length == 0) {
      this.formArray.push("");
    }
  }

  // ControlValueAccessor implementation
  propagateChange = (_: any) => { }
  writeValue(value: any): void {}
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  registerOnTouched(fn: any): void {}
  //setDisabledState?(isDisabled: boolean): void {}

  // InputMultiComponent implementation
  removeAt(index: number): void {
    this.formArray.removeAt(index);
  }

  at(index: number): string {
    return this.formArray.at(index).value;
  }

  insertAt(index: number, value: AbstractControl | string): void {
    this.formArray.insert(index, value);
  }

  push(value: AbstractControl | string): void {
    this.formArray.push(value);
  }

  set(value: string[]): void;
  set(value: AbstractControl[]): void;
  set(value: any[]): void {
    this.formArray.setValue(value)
  }

  clear() {
    while(this.formArray.length > 1)
      this.formArray.removeAt(1);
    this.formArray.controls[0].setValue("")
  }
}
